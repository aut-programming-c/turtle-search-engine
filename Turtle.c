#include <dirent.h>
#include <errno.h>
#include <sys/types.h>
#include <stdio.h>
#include<string.h>
#include<windows.h>
#include <time.h>
#define MAXN 1000
///"C://Users//salam//Desktop//Turtle//Directory"

struct primery_node{
    char word[MAXN];
    char address[MAXN];
    int line;
    struct primery_node* next;
};
struct primery_node* head_primery=NULL;

struct file_node{
    char address[MAXN];
    int line;
    struct file_node*next;
};
struct file_node* head_text=NULL;

struct node{
    char word[MAXN];
    int repeat;
    struct file_node* file;
    struct node* left;
    struct node* right;

};
struct node* head_tree=NULL;

struct search_file{
    int num_file;
    char file[100][100];
};
int num_word_input=0;
char word_input[MAXN][MAXN];

struct Offer{
    char word[MAXN];
    int dis;
};
char logo[9][6];

void show_turtle(int x,int y){
    int i,j;
    gotoxy(x,y);
    for(i=0 ; i<6  ;i++){
        for(j=0 ; j<9 ; j++){
            printf("%c",logo[j][i]);
        }
        puts("");
    }
}
char* only_name(char* address){
    int i,j;
    char name[MAXN];
    for(i=leng_str(address)-1 ; address[i]!='/' ; i--);

    i++;

    for(j=i ; j<leng_str(address) ; j++){
        name[j-i]=address[j];
    }
    name[j-i]='\0';
    return name;
}
int stack_main_num=0;
char stack_main[MAXN][MAXN];
void push_main(char* str){
    if(strcmp(str,"")!=0){
        copy_str(stack_main[stack_main_num],str);
        stack_main_num++;
    }
}
char* pop_main(){
    stack_main_num--;
    return stack_main[stack_main_num];
}
int stack_help_num=0;
char stack_help[MAXN];
void pop_help(){
    char str[MAXN];
    stack_help_num--;
    str[0]=stack_help[stack_help_num];
    push_main(str);
}
void push_help(char c){
    int flag=1;
    char top;
    while(flag){
        if(stack_help_num==0){
            break;
        }
        top=stack_help[stack_help_num-1];
        switch(c){
            case '(':
                flag=0;
                break;
            case ')':
                while(top!='('){
                    pop_help();
                    top=stack_help[stack_help_num-1];
                }
                stack_help_num--;
                flag=0;
                break;
            case '!':
                if(top=='!'){
                    pop_help();
                }
                else{
                    flag=0;
                }
                break;
            case '&':
                if(top=='&' || top=='!'){
                    pop_help();
                }
                else{
                    flag=0;
                }
                break;
            case '|':
                if(top=='!' || top=='&' || top=='|'){
                    pop_help();
                }
                else{
                    flag=0;
                }
                break;
            case '^':
                if(top=='!' || top=='&' || top=='|' || top=='^'){
                    pop_help();
                }
                else{
                    flag=0;
                }
                break;

        }

    }
    if(c!=')'){
        stack_help[stack_help_num]=c;
        stack_help_num++;
    }
}
int is_logical(char ch){
    if(ch=='!' || ch=='^' || ch=='|' || ch=='&'  || ch=='(' || ch==')'){
        return 1;
    }
    return 0;
}
int check_complex(char* str){
    int i,j;
    int num_baz=0;
    gotoxy(0,7);
    for(i=0 ; str[i]!='\0' ; i++){
        if(str[i]==' '){
            printf("Syntax ERROR :You should not enter space\n");
            return 0;
        }
    }
    for(i=0 ; str[i]!='\0' ; i++){
        if(str[i]=='('){
            num_baz++;
            if(i>0 && is_logical(str[i-1])==0){
                printf("Syntax ERROR :Before parentheses there is not a logical character\n");
                return 0;
            }
        }
        else if(str[i]==')'){
            num_baz--;
            if(num_baz<0){
                printf("Syntax ERROR :Parentheses\n");
                return 0;
            }
        }
    }
    if(num_baz!=0){
        printf("Syntax ERROR : Number of parentheses Wrong\n");
        return 0;
    }
    for(i=1 ; str[i]!='\0' ; i++){
        if((str[i]=='|' || str[i]=='&' || str[i]=='^')&&(str[i-1]=='|' || str[i-1]=='&' || str[i-1]=='^')){
           printf("Syntax ERROR: Both of two logical characters: %c%c\n",str[i-1],str[i]);
           return 0;
        }
    }
    if(str[0]=='|' || str[0]=='&' || str[0]=='^'){
        printf("Syntax ERROR : First Character not valid %c\n",str[0]);
        return 0;
    }
    for(i=1 ; str[i]!='\0' ;i++){
        if((str[i]=='|' || str[i]=='&' || str[i]=='^')&& str[i-1]=='('){
            printf("Syntax ERROR : First character after Parentheses not valid %c%c\n",str[i-1],str[i]);
            return 0;
        }
    }
    if(str[leng_str(str)-1]=='!' || str[leng_str(str)-1]=='&' || str[leng_str(str)-1]=='|' || str[leng_str(str)-1]=='^'){
        printf("Syntax ERROR : Last character not valid %c\n",str[i]);
        return 0;
    }
    for(i=1 ; str[i]!='\0' ; i++){
        if(str[i]=='!' && is_logical(str[i-1])==0){
            printf("Syntax ERROR :Character '!' used wrong\n");
            return 0;
        }
    }
    return 1;
}

int leng_str(char* str_inp){
    int i;
    for(i=0 ; str_inp[i]!='\0' ; i++);
    return i;
}
void combine_str(char* str1,char* str2){
    int i;
    int siz=leng_str(str1);
    for(i=siz  ; i<=siz+leng_str(str2) ; i++){
        str1[i]=str2[i-siz];
    }
}
void copy_str(char* str1,char* str2){
    int i;
    for(i=0 ; i<=leng_str(str2) ; i++){
        str1[i]=str2[i];
    }
}
int distance_words(char* str1,char* str2){
    int len1,len2;
    int i,j;
    len1=leng_str(str1);
    len2=leng_str(str2);
    int table[100][100];
    int cost;
    if(len1==0){
        return len2;
    }
    if(len2==0){
        return len1;
    }
    for(j=0 ; j<=len2 ; j++){
        table[0][j]=j;
    }
    for(i=0 ; i<=len1 ; i++){
        table[i][0]=i;
    }
    for(i=1 ; i<=len1 ; i++){
        for(j=1 ; j<=len2 ; j++){
            if(str1[i-1]==str2[j-1]){
                cost=0;
            }
            else{
                cost=1;
            }
            if(table[i][j-1]<table[i-1][j]){
                if(table[i-1][j-1]+cost<table[i][j-1]+1){
                    table[i][j]=table[i-1][j-1]+cost;
                }
                else{
                    table[i][j]=table[i][j-1]+1;
                }
            }
            else{
                if(table[i-1][j-1]+cost<table[i-1][j]+1){
                    table[i][j]=table[i-1][j-1]+cost;
                }
                else{
                    table[i][j]=table[i-1][j]+1;
                }
            }
        }
    }
    return table[len1][len2];

}

struct Offer offer[3];
void find_similar(char* input_word,struct node* root){
    int dis;
    struct Offer tmp;
    if(root==NULL){
        return;
    }
    else{
        dis=distance_words(input_word,root->word);
        if(dis<offer[2].dis){
            copy_str(tmp.word,root->word);
            tmp.dis=dis;
            offer[2]=tmp;
            if(offer[2].dis<offer[1].dis){
                tmp=offer[2];
                offer[2]=offer[1];
                offer[1]=tmp;
                if(offer[1].dis<offer[0].dis){
                    tmp=offer[1];
                    offer[1]=offer[0];
                    offer[0]=tmp;
                }
            }

        }
        find_similar(input_word,root->left);
        find_similar(input_word,root->right);
    }
}
struct search_file solve_postfix(){
    struct search_file file1,file2,file3;
    char str[MAXN];
    struct node* cn=head_tree;
    struct file_node* fcn;
    int i,j,flag;
    int num_text[MAXN];
    file1.num_file=0;
    copy_str(str,pop_main());
    if(!is_logical(str[0])){
            flag=1;
            for(i=0 ; i<num_word_input ; i++){
                if(strcmp(word_input[i],str)==0){
                    flag=0;
                    break;
                }
            }
            if(flag){
                copy_str(word_input[num_word_input],str);
                num_word_input++;
            }

            while(cn!=NULL){
                if(strcmp(str,cn->word)==0){
                    fcn=cn->file;
                    while(fcn!=NULL){
                        flag=1;
                        for(i=0 ; i<file1.num_file ; i++){
                            if(strcmp(file1.file[i],fcn->address)==0){
                                flag=0;
                                break;
                            }
                        }
                        if(flag){
                            copy_str(file1.file[file1.num_file],fcn->address);
                            file1.num_file++;
                        }
                        fcn=fcn->next;
                    }/*
                    for(i=0 ; i<file1.num_file ; i++){
                        printf("%s\n",file1.file[i]);
                    }*/

                    return file1;
                }
                else if(strcmp(str,cn->word)>0){
                    cn=cn->right;
                }
                else{
                    cn=cn->left;
                }
            }
            return file1;
    }
    else{
       // printf("char:%c\n",str[0]);
        if(str[0]=='!'){
            file2=solve_postfix();
        }
        else{
            file2=solve_postfix();
            file3=solve_postfix();
        }

        switch(str[0]){
            case '!':
                fcn=head_text;
                while(fcn!=NULL){
                    flag=1;
                    for(i=0 ; i<file2.num_file ; i++){
                        if(strcmp(file2.file[i],fcn->address)==0){
                            flag=0;
                            break;
                        }
                    }
                    if(flag==1){
                        copy_str(file1.file[file1.num_file],fcn->address);
                        file1.num_file++;
                    }
                    fcn=fcn->next;
                }
                return file1;
                break;
            case '&':
                for(i=0 ; i<file2.num_file ; i++){
                    for(j=0 ; j<file3.num_file ; j++){
                        if(strcmp(file2.file[i],file3.file[j])==0){
                            copy_str(file1.file[file1.num_file],file2.file[i]);
                            file1.num_file++;
                            break;
                        }
                    }
                }
                return file1;
                break;
            case '^':
                for(i=0 ; i<file2.num_file ; i++){
                    copy_str(file1.file[file1.num_file],file2.file[i]);
                    file1.num_file++;
                }
                for(j=0 ; j<file3.num_file ; j++){
                    flag=1;
                    for(i=0 ; i<file1.num_file ; i++){
                        if(strcmp(file3.file[j],file1.file[i])==0){
                            flag=0;
                            break;
                        }
                    }
                    if(flag){
                        copy_str(file1.file[file1.num_file],file3.file[j]);
                        file1.num_file++;
                    }
                }
                return file1;
                break;
            case '|':
                for(i=0 ; i<file2.num_file ; i++){
                    flag=1;
                    for(j=0 ; j<file3.num_file ; j++){
                        if(strcmp(file2.file[i],file3.file[j])==0){
                            flag=0;
                            break;
                        }
                    }
                    if(flag){
                        copy_str(file1.file[file1.num_file],file2.file[i]);
                        file1.num_file++;
                    }
                }
                for(j=0 ; j<file3.num_file ; j++){
                    flag=1;
                    for(i=0 ; i<file2.num_file ; i++){
                        if(strcmp(file2.file[i],file3.file[j])==0){
                            flag=0;
                            break;
                        }
                    }
                    if(flag){
                        copy_str(file1.file[file1.num_file],file3.file[j]);
                        file1.num_file++;
                    }
                }
                return file1;
                break;
        }
    }
    //struct search_file file1;///***
    //return file1;///***
}

void sub_search(char* address){
    struct file_node* fcn;
    struct node* cn;
    int repeat;
    int i;
    for(i=0 ; i<num_word_input ; i++){
        cn=head_tree;
        while(cn!=NULL){
            if(strcmp(cn->word,word_input[i])==0){
                repeat=0;
                fcn=cn->file;
                while(fcn!=NULL){
                    if(strcmp(fcn->address,address)==0){
                        repeat++;
                    }
                    fcn=fcn->next;
                }
                if(repeat>0){
                    printf(" ->%s : %d\n",cn->word,repeat);
                }
                break;
            }
            else if(strcmp(cn->word,word_input[i])<0){
                cn=cn->right;
            }
            else{
                cn=cn->left;
            }
        }
    }
}
void search_single(char* str){
    struct node* cn=head_tree;
    struct file_node* fcn;
    int simil;

    clock_t start, end;
    double cpu_time_used;
    start = clock();

    while(cn!=NULL){
        if(strcmp(cn->word,str)==0){
            fcn=cn->file;

            end = clock();
            cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
            gotoxy(8,3);
            printf("Time left: %lf sec\n",cpu_time_used);
            gotoxy(8,4);
            puts("Turtle found your word :) ");
            gotoxy(0,7);
            printf("\"%s\" found %d place(s) in your directory\n",str,cn->repeat);
            puts("\n*******************************************************");
            while(fcn!=NULL){
                printf("%s ::",only_name(fcn->address));
                line_file(fcn->address,fcn->line);
                puts("");
                fcn=fcn->next;
            }
            puts("*******************************************************\n");

            return;
        }
        else if(strcmp(cn->word,str)<0){
            cn=cn->right;
        }
        else{
            cn=cn->left;
        }
    }

    offer[0].dis=MAXN; offer[1].dis=MAXN; offer[2].dis=MAXN;
    copy_str(offer[0].word,"###");
    copy_str(offer[1].word,"###");
    copy_str(offer[2].word,"###");

    find_similar(str,head_tree);



    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    gotoxy(8,3);
    printf("Time left: %lf sec\n",cpu_time_used);

    gotoxy(8,4);
    printf("Turtle could not found your word ): ");
    gotoxy(0,7);

    puts("\n*******************************************************");
    puts("Did you mean?");
    printf("1.%s\n",offer[0].word);
    printf("2.%s\n",offer[1].word);
    printf("3.%s\n",offer[2].word);
    printf("4.No....!\n");
    puts("\n\n*******************************************************");
    gotoxy(0,14);
    scanf("%d",&simil);
    switch(simil){
        case 1:{
            search(offer[0].word);
            break;
        }
        case 2:{
            search(offer[1].word);
            break;
        }
        case 3:{
            search(offer[2].word);
            break;
        }
        case 4:{
            gotoxy(0,18);
            break;
        }

    }
}
void search_complex(char* inp){
    char str[MAXN][MAXN];
    int i,j,h;
    int num_part=0;
    struct search_file ans_file;

    if(check_complex(inp)==0){
        return;
    }

    clock_t start, end;
    double cpu_time_used;
    start = clock();

    num_word_input=0;
    for(i=0,j=0 ; inp[i]!='\0' ; i++,j++){
        if(is_logical(inp[i])){
            for(h=0 ; h<j ; h++){
                str[num_part][h]=inp[i-j+h];
            }
            str[num_part][h]='\0';
            num_part++;
            str[num_part][0]=inp[i];
            num_part++;
            j=-1;
        }
    }
    for(h=0 ; h<j ; h++){
        str[num_part][h]=inp[i-j+h];
    }
    str[num_part][h]='\0';
    num_part++;
    for(i=0 ; i<num_part ; i++){
        if(is_logical(str[i][0])){
            push_help(str[i][0]);
        }
        else{
            push_main(str[i]);
        }
    }
    while(stack_help_num){
        pop_help();
    }

    ans_file=solve_postfix();



    puts("\n*******************************************************");
    for(i=0 ; i<ans_file.num_file ; i++){
        printf("%s\n",only_name(ans_file.file[i]));
        sub_search(ans_file.file[i]);
    }
    puts("*******************************************************\n");
    gotoxy(8,3);
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Time left: %lf sec\n",cpu_time_used);
    gotoxy(0,9+i*3);
}
void search(char*inp){
    int i;
    system("cls");
    show_turtle(0,0);
    gotoxy(10,1);
    printf("TURTLE");
    gotoxy(8,2);
    printf("input:%s",inp);
    for(i=0 ; inp[i]!='\0' ; i++){
        if(is_logical(inp[i])){
            search_complex(inp);
            return;
        }
    }
    search_single(inp);
}
void make_letter_small(char* str){
    int i=0;
    for(i=0 ; i<leng_str(str) ; i++){
        if('A'<=str[i] && str[i]<='Z'){
            str[i]=str[i]-'A'+'a';
        }
    }
}
void read_text(char* address){
    FILE* fp=fopen(address,"r");
    struct primery_node* new_node;
    struct primery_node* cn;
    struct file_node* fcn;      ///***
    struct file_node* new_text; ///***
    int line=1;
    char inp[MAXN];

    new_text=(struct file_node*)malloc(sizeof(struct file_node));
    copy_str(new_text->address,address);
    new_text->next=NULL;
    if(head_text==NULL){
        head_text=new_text;
    }
    else{
        fcn=head_text;
        while(fcn->next!=NULL){
            fcn=fcn->next;
        }
        fcn->next=new_text;
    }
    while(!feof(fp)){
        fscanf(fp,"%s",inp);
        make_letter_small(inp);
        if(strcmp(inp,".")==0){
            line++;
        }
        else if(is_skipword(inp)==0){
            new_node=(struct primery_node*)malloc(sizeof(struct primery_node));
            copy_str(new_node->word,inp);
            copy_str(new_node->address,address);
            new_node->line=line;
            new_node->next=NULL;
            if(head_primery==NULL){
                head_primery=new_node;
            }
            else{
                cn=head_primery;
                while(cn->next!=NULL){
                    cn=cn->next;
                }
                cn->next=new_node;
            }
        }
    }

}
void read_directory(char* main_dir){
    DIR *dir;
    struct dirent *file;
    char fileName[MAXN];
    dir = opendir(main_dir);

    int siz;
    char new_dir[MAXN];
    char new_text[MAXN];
    while ((file = readdir(dir)) != NULL)
    {
        //printf("  %s\n", file->d_name);
        strncpy(fileName, file->d_name, MAXN-1);
        //fileName[MAXN-1] = '\0';
        if(fileName[0]!='.'){
            siz=leng_str(fileName);
            if(siz>=4 && '.'==fileName[siz-4] && 't'==fileName[siz-3] && 'x'==fileName[siz-2] && 't'==fileName[siz-1]){
                copy_str(new_text,main_dir);
                combine_str(new_text,"//");
                combine_str(new_text,fileName);
                read_text(new_text);
            }
            else{
                copy_str(new_dir,main_dir);
                combine_str(new_dir,"//");
                combine_str(new_dir,fileName);
                read_directory(new_dir);
            }
        }

    }
    closedir(dir);
}
char skipword[50][100*1000];
int num_skipword=0;
void read_skipword(){
    char address[MAXN];
    FILE* fp;
    //puts("Enter SkipWord");
    //scanf("%s",address);
    copy_str(address,"SkipWord.txt");
    fp=fopen(address,"r");
    while(!feof(fp)){
        fscanf(fp,"%s",skipword[num_skipword]);
        num_skipword++;
    }
}
int is_skipword(char* str){
    int i;
    for(i=0 ; i<num_skipword ; i++){
        if(strcmp(str,skipword[i])==0){
            return 1;
        }
    }
    return 0;
}
void delete_primery(){
    struct primery_node* cn;
    struct primery_node* pn;
    cn=head_primery;
    while(cn!=NULL){
        pn=cn;
        cn=cn->next;
        free(pn);
    }
}
struct node* make_tree_search(char* str){
    struct node* cn=head_tree;
    if(head_tree==NULL){
        return NULL;
    }
    else{
        while(cn!=NULL){
            if(strcmp(cn->word,str)==0){
                return cn;
            }
            else if(strcmp(cn->word,str)<0){
                cn=cn->right;
            }
            else{
                cn=cn->left;
            }
        }
        return NULL;
    }
};

void end_program(){
    system("cls");
    printf("%c",7);
    show_turtle(0,0);

    gotoxy(10,1);
    printf("TURTLE");
    gotoxy(8,2);
    printf("A search application");
    gotoxy(8,3);
    printf("Created by Alireza Mazochi");
    gotoxy(8,4);
    printf("1396/11/2");

    system("Color 24");
    Sleep(50);
    system("Color 25");
    Sleep(50);
    system("Color 27");
    Sleep(50);
    system("Color 2F");
    Sleep(50);
    system("Color 29");
    Sleep(50);
    system("Color 2E");
    Sleep(50);
    system("Color 24");
    Sleep(50);
    system("Color 25");
    Sleep(50);
    system("Color 27");
    Sleep(50);
    system("Color 2F");
    Sleep(50);
    system("Color 29");
    Sleep(50);
    system("Color 2E");
    Sleep (2000);

    gotoxy(0,10);
}
void make_tree(){
    struct primery_node* cnp;
    struct node* new_node;
    struct node* cn;
    struct node* pn;
    int dir_tree;
    struct file_node* new_file;
    struct file_node* cnf;
    if(head_primery==NULL){
        puts("Directory is not available or empty of texts");
        Sleep(2000);
        end_program();
        exit(0);
        return;
    }
    cnp=head_primery;
    while(cnp!=NULL){
        new_node=make_tree_search(cnp->word);
        if(new_node==NULL){
            new_node=(struct node*)malloc(sizeof(struct node));
            new_file=(struct file_node*)malloc(sizeof(struct file_node));

            copy_str(new_node->word,cnp->word);
            new_node->repeat=1;
            new_node->file=new_file;
            copy_str(new_file->address,cnp->address);
            new_file->line=cnp->line;
            new_file->next=NULL;

            if(head_tree==NULL){
                head_tree=new_node;
            }
            else{
                pn=NULL;
                cn=head_tree;
                dir_tree=0;
                while(cn!=NULL){
                    if(strcmp(cnp->word,cn->word)>0){
                        dir_tree=2;
                        pn=cn;
                        cn=cn->right;
                    }
                    else{
                        dir_tree=1;
                        pn=cn;
                        cn=cn->left;
                    }
                }
                if(dir_tree==1){
                    pn->left=new_node;
                }
                else{
                    pn->right=new_node;
                }
            }

        }
        else{
            new_file=(struct file_node*)malloc(sizeof(struct file_node));
            copy_str(new_file->address,cnp->address);
            new_file->line=cnp->line;
            new_file->next=NULL;
            cnf=new_node->file;
            new_node->repeat++;
            while(cnf->next!=NULL){
                cnf=cnf->next;
            }
            cnf->next=new_file;
        }
        cnp=cnp->next;
    }
}
void line_file(char* address,int line){
    FILE* fp=fopen(address,"r");
    int i=0;
    char c;
    while(i<line-1){
        c=fgetc(fp);
        if(c=='.'){
            i++;
        }
    }
    if(line>1){
        fgetc(fp);
    }
    while(1){
        c=fgetc(fp);
        printf("%c",c);
        if(c=='.'){
            puts("");
            fclose(fp);
            return;
        }
    }
}
void gotoxy( short x, short y ){
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE) ;
    COORD position = { x, y } ;
    SetConsoleCursorPosition( hStdout, position ) ;
}

void show_history(){


}
void start_program(){
    char main_dir[MAXN];
    system("Color 2E");
    printf("Enter your directory : ");
    scanf("%s",main_dir);
    read_skipword();
    puts("Loading...");

    clock_t start, end;
    double cpu_time_used;
    start = clock();

    read_directory(main_dir);
    make_tree();
    delete_primery();
    logo[0][0]=' '; logo[1][0]=' '; logo[2][0]=' '; logo[3][0]=219; logo[4][0]=219; logo[5][0]=219; logo[6][0]=' '; logo[7][0]=' ';
    logo[0][1]=219; logo[1][1]=219; logo[2][1]=' '; logo[3][1]=219; logo[4][1]=219; logo[5][1]=219; logo[6][1]=' '; logo[7][1]=219; logo[8][1]=219;
    logo[0][2]=' '; logo[1][2]=' '; logo[2][2]=219; logo[3][2]=177; logo[4][2]=219; logo[5][2]=177; logo[6][2]=219; logo[7][2]=' ';
    logo[0][3]=' '; logo[1][3]=' '; logo[2][3]=177; logo[3][3]=219; logo[4][3]=177; logo[5][3]=219; logo[6][3]=177; logo[7][3]=' ';
    logo[0][4]=' '; logo[1][4]=' '; logo[2][4]=219; logo[3][4]=177; logo[4][4]=219; logo[5][4]=177; logo[6][4]=219; logo[7][4]=' ';
    logo[0][5]=219; logo[1][5]=219; logo[2][5]=' '; logo[3][5]=219; logo[4][5]=177; logo[5][5]=219; logo[6][5]=' '; logo[7][5]=219; logo[8][5]=219;
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Time left: %lf sec\n",cpu_time_used);
    puts("Done!");
    Sleep(1500);
    system("cls");
}
int main(){
    char inp[MAXN];
    start_program();
    while(1){
        puts("Enter word!");
        scanf("%s",inp);
        if(strcmp(inp,"Terminate")==0){
            end_program();
            return 0;
        }
        else if(strcmp(inp,"History")==0){
            show_history();
        }
        else{
            search(inp);
        }
    }
    return 0;
}





