#include<stdio.h>

int stack_main_num=0;
char stack_main[1000];
void push_main(char x){
    stack_main[stack_main_num]=x;
    stack_main_num++;
}

char pop_main(){
    stack_main_num--;
    return stack_main[stack_main_num];
}
int stack_help_num=0;
char stack_help[1000];
void pop_help(){
    stack_help_num--;
    push_main(stack_help[stack_help_num]);
}
void push_help(char c){
    int flag=1;
    char top;
    while(flag){
        if(stack_help_num==0){
            break;
        }
        top=stack_help[stack_help_num-1];
        switch(c){
            case '(':
                flag=0;
                break;
            case ')':
                while(top!='('){
                    pop_help();
                    top=stack_help[stack_help_num-1];
                    //printf("%d %c\n",stack_help_num,top);
                }
               // printf("%d\n",stack_help_num);
                stack_help_num--;
                flag=0;
                break;
            case '*':
                if(top=='*' || top=='/'){
                    pop_help();
                }
                else{
                    flag=0;
                }
                break;
            case '/':
                if(top=='*' || top=='/'){
                    pop_help();
                }
                else{
                    flag=0;
                }
                break;
            case '+':
                if(top=='*' || top=='/' || top=='+' || top=='-'){
                    pop_help();
                }
                else{
                    flag=0;
                }
                break;
            case '-':
                if(top=='*' || top=='/' || top=='+' || top=='-'){
                    pop_help();
                }
                else{
                    flag=0;
                }
                break;

        }

    }
    if(c!=')'){
        stack_help[stack_help_num]=c;
        stack_help_num++;
    }
}
float calc_postfix(){
    char ch1,ch2,ch3;
    ch1=pop_main();
   // printf(":%c\n",ch1);
    if(('0'<=ch1 && ch1<='9')){
        return ch1-'0';
    }
    else{
        ch2=calc_postfix();
        ch3=calc_postfix();
    }
    switch(ch1){
        case '+':
            //printf("+");
            return (ch2)+(ch3);
            break;
        case '-':
            //printf("-");
            return (ch2)-(ch3);
            break;
        case '*':
            //printf("*");
            return (ch2)*(ch3);
            break;
        case '/':
            //printf("/");
            return (ch3)/(ch2);
            break;
    }
 }
int main(){
    char inp[1000];
    int i;
    scanf("%s",inp);
    for(i=0 ; inp[i]!='\0' ; i++){
        if('0'<= inp[i] && inp[i]<='9'){
            push_main(inp[i]);
        }
        else{
            push_help(inp[i]);
        }
    }

    while(stack_help_num){
        pop_help();
    }
    for(i=0 ; i<stack_main_num ; i++){
        printf("%c",stack_main[i]);
    }
    printf("\n%f",calc_postfix());
    return 0;
}
